<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/users', 'UserController@store');
Route::post('/posts', 'PostController@store');
Route::post('/comments', 'CommentController@store');
Route::get('/users/{user}/posts', 'UserController@posts')->name('api.user.posts');
Route::get('/postscomments/{post}', 'PostController@postWithComments');
Route::put('/posts/{post}', 'PostController@update');


Route::get('/ivan/{post}', 'PostController@ivan');
