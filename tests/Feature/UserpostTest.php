<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Post;

class UserpostTest extends TestCase
{
    /** @test */
    public function it_returns_the_list_of_posts_created_by_a_user()
    {
        //preparar la data
        //crear un usuario
        //Crear dos o tres posts
        $user = factory(User::class)->create();
        $posts = factory(Post::class, 3)->create([
            'user_id' => $user
        ]);
        //actuar - accion que queres testear
        $response = $this->get(route('api.user.posts', $user));

        //assertar - verificar lo que te retorno
        $response->assertStatus(200);
        $response->assertJson($posts->toArray());
    }
    
}
