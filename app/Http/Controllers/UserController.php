<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function store()
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email'
        ]);    

        User::create([
            'name' => request('name'),
            'email' => request('email')
        ]);

        return response('Hello World', 200);    
    }

    public function posts(User $user)
    {
        return $user->posts;
    }
}
