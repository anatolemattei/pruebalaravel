<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    public function store()
    {
        request()->validate([
            'message' => 'required',
            'user_id' => 'required|exists:users,id',
            'post_id' => 'required|exists:posts,id'
        ]);    

        $comment = Comment::create([
            'message' => request('message'),
            'user_id' => request('user_id'),
            'post_id' => request('post_id')
        ]);

        return response($comment);    
    }
}
