<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function store()
    {
        request()->validate([
            'title' => 'required',
            'body' => 'required',
            'user_id' => 'required'
        ]);    

        $post = Post::create([
            'title' => request('title'),
            'body' => request('body'),
            'user_id' => request('user_id|exists:users,id')
        ]);

        return response($post, 200);    
    }

    public function update(Post $post)
    {
        //dd($post);
        request()->validate([
            'title' => 'required',
            'body' => 'required',
            'user_id' => 'required|exists:users,id'
        ]);
        
        $post->title = request('title');
        $post->body = request('body');
        $post->user_id = request('user_id');
        $post->save();

        return response($post, 200);
    }

    public function postWithComments(Post $post)
    {
        $post->comments;
        return response($post);
    }

    public function ivan(Post $post)
    {
        dd($post->comments);
        return response('bla');
    }
}
